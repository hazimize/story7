$(function(){

    $('#accordion').accordion({
        header: '> div.wrapper > h3 ',
        collapsible:true,
        animate:100,
        heightStyle:'content',
        active: true,
    })

})

window.foo = function(e) {
    e.stopPropagation();
  
    $('.up').on('click', function () {
        var thisAccordion = $(this).parent().parent().parent();
        console.log(thisAccordion);
        thisAccordion.insertBefore(thisAccordion.prev());

    });
    $('.down').on('click', function () {
        var thisAccordion = $(this).parent().parent().parent();
        console.log(thisAccordion);
        thisAccordion.insertAfter(thisAccordion.next());

    });

}
